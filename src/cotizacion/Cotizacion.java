/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotizacion;

/**
 *
 * @author Edgar Guerrero
 */
public class Cotizacion {
    private int numCotizacion;
    private String descripcionAutomovil;
    private float precio;
    private float porcentajePagoInicial;
    private int plazo; 
    
    //constructores

    public Cotizacion() {
        
    }

    public Cotizacion(int numCotizacion, String descripcionAutomovil, float precio, float porcentajePagoInicial, int plazo) {
        this.numCotizacion = numCotizacion;
        this.descripcionAutomovil = descripcionAutomovil;
        this.precio = precio;
        this.porcentajePagoInicial = porcentajePagoInicial;
        this.plazo = plazo;
    }
    
    public Cotizacion( Cotizacion otro ){
        this.numCotizacion = otro.numCotizacion;
        this.descripcionAutomovil = otro.descripcionAutomovil;
        this.precio = otro.precio;
        this.porcentajePagoInicial = otro.porcentajePagoInicial;
        this.plazo = otro.plazo;
    }
    
        //metodos set/get
    
    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcionAutomovil() {
        return descripcionAutomovil;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(float porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
        //metodos de comportamiento
    public float calcularPagoInicial(){
        float pagoInicial = 0.0f;
        pagoInicial = this.porcentajePagoInicial * this.precio / 100f;
        return pagoInicial;
    }
    public float calcularTotalFinancial(){
        float totalFinancial = 0.0f;
        totalFinancial = this.precio - this.calcularPagoInicial();
        return totalFinancial;
    }
    public float calcularPagoMensual(){
        float pagoMensual = 0.0f;
        pagoMensual = this.calcularTotalFinancial()/ this.plazo;
        return pagoMensual;
    }
    
    public void imprimirCotizacion(){
        System.out.println("Numero de Cotizacion: " + this.numCotizacion);
        System.out.println("Descripcion: " + this.descripcionAutomovil);
        System.out.println("Precio del Auto: " + this.precio);
        System.out.println("Porcentaje de pago inicial: " + this.porcentajePagoInicial);
        System.out.println("Plazo: " + this.plazo);
        System.out.println("Pago inicial: " + this.calcularPagoInicial());
        System.out.println("Total financiar: " + this.calcularTotalFinancial());
        System.out.println("Pago Mensual: " + this.calcularPagoMensual());
        
    }

    void setPorcentaje(float parseFloat) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}